#include <iostream>
#include <list>
#include <vector>
#include "graph.h"
using namespace std;

int main()
{
	int arr[4][4], weight, i, j;
	Graph graph;

	for (i = 0; i < 4; i++)
	{
		for (j = 0; j < 4; j++)
		{
			cout << "Input weight for each egde[" << i << " to " << j << "] ";
			cin >> weight;  //input 
			arr[i][j] = weight;
		}
	}

	system("cls");

	for (i = 0; i < 4; i++)
	{
		for (j = 0; j < 4; j++)
		{
			cout << arr[i][j] << " ";
		}
		cout << endl;
	}


	
	graph.graphForm(arr, 4);
	graph.showGraph();

	if (graph.weightGraph() == true) //check if it is weight graph
	{
		cout << "It is Weight graph" << endl;
	}


	if (graph.completedGraph() == true) //check if it is completed graph
	{
		cout << "It is Completed graph " << endl;
	}


	if (graph.pseudoGraph() == true)
	{
		cout << "It is Pseudograph" << endl;
	}


	if (graph.digraph() == true)
	{
		cout << "It is Digraph" << endl;
	}

	
	if (graph.multiGraph() == true)
	{
		cout << "It is Multigraph" << endl;
	}

	char name;
	cout << endl << "DYKSTRA for : ";
	cin >> name;
	graph.dykstra(name);
}